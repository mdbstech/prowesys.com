<?php


Route::get('/', 'WebsiteController@home');

Route::get('/home', 'WebsiteController@home');

Route::get('/about-us', 'WebsiteController@about');

Route::get('/contact-us', 'WebsiteController@contact');

Route::get('/careers', 'WebsiteController@careers');

Route::get('/industry', 'WebsiteController@industry');

//

Route::get('/enterprise_mobility','WebsiteController@enterprise_mobility');
Route::get('/enterprise_app_management','WebsiteController@enterprise_app_management');
Route::get('/application_maintenace','WebsiteController@application_maintenace');
Route::get('/mechanical_services','WebsiteController@mechanical_services');
Route::get('/digital_marketing','WebsiteController@digital_marketing');
Route::get('/testing_services','WebsiteController@testing_services');
Route::get('/erp_practice','WebsiteController@erp_practice');
Route::get('/outsourcing','WebsiteController@outsourcing');

//

Route::get('/technologies', 'WebsiteController@technologies');
Route::get('/oracle', 'WebsiteController@oracle');
Route::get('/opensource', 'WebsiteController@opensource');
Route::get('/sas', 'WebsiteController@sas');

//

Route::get('/iot_development', 'WebsiteController@iot_development');
Route::get('/business_analytics', 'WebsiteController@business_analytics');
Route::get('/business_intelligence', 'WebsiteController@business_intelligence');
Route::get('/bigdata', 'WebsiteController@bigdata');
Route::get('/data_warehousing', 'WebsiteController@data_warehousing');
Route::get('/devops_aws', 'WebsiteController@devops_aws');

//

Route::post('send','WebsiteController@send');

Route::get('/jobs','WebsiteController@jobs');

Route::get('/software-developers','WebsiteController@job1');

Route::get('/systems-analysts','WebsiteController@job2');