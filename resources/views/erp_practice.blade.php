@extends('layouts.app')

@section('content')

  <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/ser8.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Expertise Services</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">ERP Practice</div>
                                <h2>For total planning of your enterprise resource needs</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	
								<p>Prowesys  Information Systems expertise and experience with large corporations has enabled us to develop and provide unique and ERP: customized Enterprise Resource Planning Solutions to our customers.</p>

<p>Prowesys  Information Systems ERP : Enterprise Resource Planning Solution will cater to your company’s imperative needs of a well-planned resource utilization strategy. You will be able to manage your workforce with a better focus on the utilization front to take care of any exigencies in future.</p>

<p>Our Solution provides you with the tools to manage complex projects in a very efficient manner and hedge any risks arising out of any fluctuation in the resources or inputs.</p>

<h3>Business Value with Enterprise Resource Planning:</h3>
» Efficient management of resources
» Project Planning Methodology
» Easy management of inputs
» Comprehensive Solution for Projects
» Scalable and robustly designed
» Efficiency in the Enterprise

<div class="image">
                                	<img src="images/erp.jpg" alt="">
                                </div>
<p>Prowesys  Information Systems currently designs, supports, implements the following Enterprise Resource Planning Solutions:</p>

» SAP<br>
» JD Edwards<br>
» PeopleSoft<br>
» Oracle<br>
» Custom ERP Solutions<br><br>


<p>Prowesys  offers a few Enterprise Application Services like ERP Applications, Business Intelligence, CRM Applications, Supply Chain Management Applications and Enterprise Application Integration (EAI).</p>

<p>Our Services are ranging from Business Process Studies and Preparation of Blueprint Documents, Development, Deployment, Global Rollouts, Help Desk Support to Application Maintenance in areas such as ERP, CRM, SCM and Middleware.</p>

<p>Prowesys  provides package implementations, Rollout, Upgrade, Migration, Application Support & Maintenance and Integration Services in SAP all in one, Business one and MYSAP Business Suits. Prowesys  follows ASAP Methodologies for the SAP Solutions.</p>

<p>SAP Financial Accounting (FI), Controlling (CO), Sales & Distribution (SD), Material Management (MM), Production Planning (PP), Quality Management (QM), Plant Management (PM), HR-Payroll, Project System.</p>

<p>SAP-BW, SAP-APO, SAP-CRM, SAP-SRM, SAP-PLM, SAP-SCM, SAP-SEM.</p>

<h3>Services we offer include:</h3>
» SAP Functional<br>
» SAP project management<br>
» Custom development<br>
» Business Intelligence and Data Warehousing<br>
» Net Weaver<br>
» Offshore Development, Support, & Maintenance<br>
» Staff augmentation<br>


                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                         <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Expertise Services</h2></div>
							<div class="widget-content">
                            	<ul>
                                	  <li><a href="{{ url ('/enterprise_mobility') }}">Enterprise Mobility</a></li>
										<li><a href="{{ url ('/enterprise_app_management') }}">Enterprise Application Management</a></li>
										<li><a href="{{ url ('/application_maintenace') }}">Application Maintenance</a></li>
										<li><a href="{{ url ('/mechanical_services') }}">Mechanical Services</a></li>
										<li><a href="{{ url ('/digital_marketing') }}">Digital Marketing</a></li>
										<li><a href="{{ url ('/testing_services') }}">Testing Services</a></li>
										<li><a href="{{ url ('/erp_practice') }}">ERP Practice</a></li>
										<li><a href="{{ url ('/outsourcing') }}">Outsourcing</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection