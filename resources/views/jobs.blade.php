@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/background/9.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Jobs</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">

                     <div class="row">
                        <div class="col-lg-6">
                            <div class="approach_text">
                                <h4><b>CAREERS</b></h4><br>
                                <h4>EMPLOYMENT OPPORTUNITIES :</h4><br>

                                   <div class="form-group btn-column">
                                       <a href="{{ url ('/software-developers') }}"> <button class="theme-btn btn-style-two" style="color: black;">Software Developers</button></a>
                                    </div>

                                       <div class="form-group btn-column">
                                        <a href="{{ url ('/systems-analysts') }}"><button class="theme-btn btn-style-two" style="color: black;">Systems Analysts</button></a>
                                    </div>
         
                            </div>
                        </div>
                    
                    </div>
            </div>
        </div>
    </div>

    @endsection