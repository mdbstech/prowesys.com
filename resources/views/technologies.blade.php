@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/tech.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Technology</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Our Technology</div>
                                <h2>Our technical skills convince the success of your projects</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<p>Proficiencies in numerous Technologies, Programming Languages, Operating Systems, Frameworks, etc.</p>
								
<p>Prowesys only hires, trains and retains talented coders and developers who are committed to providing best services to clients. In doing so, the profound knowledge and proven skills in different frameworks, programming languages, platforms...are essential and prerequisite condition. Our programmers are familiar with many development frameworks including open sources, and multi-platform application development.</p>

<p>We can work on different platforms namely Windows, Linux, MacOS etc., and different programming languages: .NET, Java, Ruby, PHP, Python, C, C++, Golang, Solidity, Blockchain frameworks: Hyperledger Fabric, Hyperledger Sawtooth, Ethereum, Tendermint. Below are some basic services provided with specific programming languages at Prowesys:</p>
<div class="image">
                                	<img src="images/tech1.jpeg" alt="">
                                </div>
<p>
•	PHP: Prowesys is expert at PHP with more than 300 projects successfully completed. With PHP we can help you:<br>
o	Setup your corporate website at minimum cost to maximize your presentation to your audience using Prowesys CMS or Joomla.<br>
o	Build an online store with full features of the latest ecommerce theories to maximize your sales using Prowesys Cart or ZenCart, Magento.<br>
o	Custom develop your PHP application for using as a community website or intranet applications using Zend Framework, Drupal, Symphony, CakePHP.<br>
•	.NET: Prowesys covers the following. NET-based services:<br>
o	Develop and design secure, scalable and configurable .NET web based or client-server or desktop applications using C# or VB.NET.<br>
o	.NET application enhancement and maintenance<br>
o	.NET application integration.<br>
o	Re-engineering/migrating desktop and web applications to .NET<br>
•	C/C++: Our team's technical skills will help you to design and develop C/C++ applications for broad range of categories: <br>
o	Desktop applications.<br>
o	Software components.<br>
o	Multimedia applications.<br>
o	C++ game development.<br>
o	Image Processing Tools and file managers.<br>
o	Extendable and portable C++ software solutions<br>
o	Complicated system level programming<br>
o	Customizable C++ applications<br>
o	Advanced C++ technologies and development tools using<br>
o	Cost-efficient C++ programming<br>
•	Java: Our dedicated Java developers offer various services to our customers:<br>
o	Java applications programming for Linux, Windows, Mac OS, Mobile platform<br>
o	Java-based desktop applications development<br>
o	Custom solutions development on Java<br>
o	J2EE design patterns<br>
o	Web design and development services and games development<br>
•	Apart from that, we can utilize most common APIs such as Google apps, MAEMO, etc, and program on many frameworks such as Zend framework, Drupal, CakePHP, Struts, Hibernate<br>
</p>

                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                        <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Technology</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/technologies') }}">Overview</a></li>
									<li><a href="{{ url ('/oracle') }}">Oracle</a></li>
									<li><a href="{{ url ('/opensource') }}">Opensource</a></li>
									<li><a href="{{ url ('/sas') }}">SAS</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection