@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/ser5.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Expertise Services</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Mechanical Services</div>
                                <h2>We strong performance, productivity and retention</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<h3>CADD/CAM SERVICES</h3>

								<p>Prowesys  provides high quality, Computer-Aided Designing and Drafting (CADD) Services.</p>

<p>We are your one-stop solution for specialized services in the domain of Architectural, Structural, Electrical, Mechanical and Electronics engineering. Our cutting edge technological expertise can meet your requirements ranging from 2D Drafting, 3D Modeling, Designing to Animation and Walkthrough.</p>

<p>We believe in partnering with clients to provide sound and affordable business solutions that help increase their profitability. So, while you concentrate on your core areas of business, we will provide you CADD services that meet international standards.</p>

<p>Prowesys  is staffed by highly skilled, computer savvy professionals. Our work culture is based on a positive attitude and strong values, which contribute to our strong performance, productivity and retention.</p>

<h3>ARCHITECTURAL/CIVIL/STRUCTURAL</h3>
» Building and Survey Drawings<br>
» Structural CAD Drawings<br>
» Civil CAD Drawings<br>
» Space Planning Layouts<br>
» Foundations<br>
» Site Drainage<br>
» Site Plans<br>
» Elevations and Cross Sections<br>
» Landscape & building configurations<br><br>

<h3>ELECTRICAL</h3>
<p>Prowesys  has extensive experience in providing Electrical / Utility / Energy drawing / detailing services for large customers worldwide. We have executed large projects for TVA, BC Hydro, United Energy, Eastern Energy and others. Our teams are quite conversant in executing the following:</p>

» Lighting Diagrams<br>
» Utility Network Diagrams<br>
» Fire Alarm Systems Diagrams<br>
» Circuit Diagrams<br>
» Local Area Network Diagrams<br>
» Layout Diagrams<br>
» Traffic Controls Diagrams<br>
» Wiring and conduit Diagrams<br>
» Interconnection diagrams<br><br>

<p>Prowesys  can process & deliver single line diagrams, detail drafting of MCC, preparation of component specifications, wiring diagrams. We have considerable experience in preparation of cable layout as per customer equipment and plant layout specifications as well as cable tray and supporting structure layout details. Similarly Prowesys  can prepare earthing system layouts, provide drawings for laying out the earthing strips, earthing pits, interconnection diagrams and lightning protection system.</p>

<h3>DRAFTING AND DETAILING</h3>
<p>Prowesys  provides services in Drafting & Detailing , Geometric Tolerances, Welding symbols, Surface textures Symbols and Dimensioning with Proper Annotation of CAD drawing in accordance with world wide drafting standards (viz. ANSI, ISO, DIN, JIS, BSI, GOST, GB, IS etc.)</p>

<p>Prowesys  has been providing services in HVAC/Plumbing /Fabrication for many customers like hospitals, office buildings, retail stores, industrial facilities, and residential structures. We provide equipment layout drawings to determine the most economical and practical routing for HVAC. Duct drawings are drawn to coordinate with the structure, ceiling, lights and other architectural systems. All Mechanical equipment is drawn as per manufacturers approved submittal.</p>

» Shop detail drawings<br>
» Bill of Materials for structural steel procurement<br>
» Erection drawings<br>
» Anchor bolt plans<br>
» Field bolt/hardware list<br>
» Industrial buildings and structures<br>
» Workshops and factory sheds<br>
» Gas and liquid pipelines and pipe racks<br>
» Offshore platforms<br>
» Drafting services for HVAC<br>
» Sleeve Drawings<br><br>

<h3>2D TO 3D CONVERSION</h3>
<p>Our team’s expertise provides the facilities to create the Isometric, Orthographic, Sectional, Detailed, Perspective and Auxiliary views as per the customer’s specification and standards for the conversion of Old 2D Drawings (Paper/Microfilms/CAD package) to 3D Models.</p>


                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                       
                        <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Expertise Services</h2></div>
							<div class="widget-content">
                            	<ul>
                                	  <li><a href="{{ url ('/enterprise_mobility') }}">Enterprise Mobility</a></li>
										<li><a href="{{ url ('/enterprise_app_management') }}">Enterprise Application Management</a></li>
										<li><a href="{{ url ('/application_maintenace') }}">Application Maintenance</a></li>
										<li><a href="{{ url ('/mechanical_services') }}">Mechanical Services</a></li>
										<li><a href="{{ url ('/digital_marketing') }}">Digital Marketing</a></li>
										<li><a href="{{ url ('/testing_services') }}">Testing Services</a></li>
										<li><a href="{{ url ('/erp_practice') }}">ERP Practice</a></li>
										<li><a href="{{ url ('/outsourcing') }}">Outsourcing</a></li>
                                </ul>
                            </div>
                        </div>
                    
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

    @endsection