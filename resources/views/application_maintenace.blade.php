@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/ser4.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Expertise Services</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Application Maintenance</div>
                                <h2>Data Conversion, Migration and Maintenance</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<p>Our approach takes a complete view of technology, information design and services. We first understand domain, technology sitemap, and applications, then optimize and align team structures to deliver improved application support. With minimum downtime and volatility, your applications stay optimally geared to provide rapid responses to key change requests and business requests.</p>

<p>The data can be accessed from any part of the world with out any delay in time. The whole software will be uploaded in to a secured server which have a secondary server back up also for making the data safe.</p>

<h3>OUR APPLICATION MAINTENANCE SERVICES ARE:</h3>
» Server Side Management<br>
» Portal Maintenance<br>
» Application Maintenance<br>
» 24/7 Production Support<br>
» Security<br><br>

<p>By leveraging our delivery model, we have helped our customers achieve significant cost savings in their maintenance spends. We have established metrics-based processes to handle transition and remote delivery of maintenance. We assume total responsibility for maintaining existing IT applications and have consistently delivered high service levels. Use of exclusive and third-party tools aid us in delivering the best to our customers.</p>

<h3>BENEFITS</h3>
» Significant reduction of maintenance costs<br>
» Timely release of programs and enhancements<br>
» Improved knowledge management<br>
» Portfolio optimization<br>
» Cost variability for cyclical businesses<br>
» Ongoing alignment with industry trends<br><br>

<h3>Our Services include :</h3>
» IT and Business Process Consulting<br>
» Application Design, Development & Maintenance<br>
» Legacy Application Migration<br>
» Enterprise Application Integration<br>
» Re-engineering<br>
» Data Conversion, Migration and Maintenance<br>
» Database Administration<br>
» Security and Infrastructure support<br>
» Application Hosting<br>
» Offshore Outsourcing<br><br>

<h3>Our Services help customers to :</h3>
» Improve business focus<br>
» Achieve higher quality and productivity<br>
» Shorten project schedules<br>
» Utilize highly specialized knowledge<br>
» Add staff without overhead<br>
» Reduce overall IT cost<br>

<div class="image">
                                	<img src="images/app-mantinance.jpg" alt="">
                                </div> 
                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                          <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Expertise Services</h2></div>
							<div class="widget-content">
                            	<ul>
                                	  <li><a href="{{ url ('/enterprise_mobility') }}">Enterprise Mobility</a></li>
										<li><a href="{{ url ('/enterprise_app_management') }}">Enterprise Application Management</a></li>
										<li><a href="{{ url ('/application_maintenace') }}">Application Maintenance</a></li>
										<li><a href="{{ url ('/mechanical_services') }}">Mechanical Services</a></li>
										<li><a href="{{ url ('/digital_marketing') }}">Digital Marketing</a></li>
										<li><a href="{{ url ('/testing_services') }}">Testing Services</a></li>
										<li><a href="{{ url ('/erp_practice') }}">ERP Practice</a></li>
										<li><a href="{{ url ('/outsourcing') }}">Outsourcing</a></li>
                                </ul>
                            </div>
                        </div>
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>
    
@endsection