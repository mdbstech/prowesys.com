@extends('layouts.app')

@section('content')

<!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/oracle.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Technology</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Oracle</div>
                                <h2>Our technical skills convince the success of your projects</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<p>Prowesys  combines technical sand industry leading practices to deliver successful business solutions and enables maximized return on investments for your Oracle applications. Leaveraging its extensive experience across the various Oracel applications and rich domain expertise, Prowesys  enables you to use Oracle applications for your business advantage.</p>

<p>Prowesys  Oracle applications COE provides Oracle consulting sergices to the world's leading corporations across various industry verticals. Equipped with robust tools, methodologies, templates, and accelerators and supported by an Oracle center of excellence, Prowesys  Oracle Practice ensures that the customer get robust implementations, faster rollouts, de-risked upgrades, dependable production support and best quality development.</p>

» Oracle Database<br>
» Oracle Applications<br>
» Oracle Fusion Applications<br>
» Oralce E-Business Suite<br>
» PeopleSoft Enterprise<br>
» Siebel<br>
» JD Edwards<br>
» Hyperion<br>
» Customer Relationship Management<br>
» Financial Management<br>
» Human Capital Management<br>
» Master Data Management<br><br>

<p>» Our Oracle Enterprise Application Competency Group has capabilities to provide ERP, CRM, and Strategic Enterprise Management (SEM) and DSS Life Cycle Implementation processes for the Oracle suite of Enterprise Applications products. We have the capabilities to provide the complete Life Cycle Implementation process for the Oracle Enterprise Application Products.</p>
<p>» Trained and experienced group of Oracle Applications ERP Consultants with the relevant skills such as Functional, Technical, and Business Analysis, and Project Management focusing exclusively on Oracle Enterprise Applications.</p>
<p>» You're the expert in your business. You understand your processes and how they work. And you also know your company goals.</p>
<p>» Finding a technology platform and business partner to support those goals isn't as clear. It's not always obvious how you can use technology best practices to improve the efficiency and productivity of your business.</p>
<p>» Oracle E-Business Suite (EBS) provides global, integrated business applications that drive business performance. Whether you're new to Oracle applications or have an older EBS installation you would like to upgrade, the functionality and support available in the newer versions of Oracle EBS applications can help you achieve your business goals.</p>
<p>» We are proud to say that we have 100% Oracle E business implementation history and we are providing the Excellent Production Support and we are sure that we can reach your expiations in E-Business Technology.</p>

                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                       
                        <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Technology</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/technologies') }}">Overview</a></li>
									<li><a href="{{ url ('/oracle') }}">Oracle</a></li>
									<li><a href="{{ url ('/opensource') }}">Opensource</a></li>
									<li><a href="{{ url ('/sas') }}">SAS</a></li>
                                </ul>
                            </div>
                        </div>
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection