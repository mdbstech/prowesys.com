@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/ser9.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Expertise Services</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Outsourcing</div>
                                <h2>we serve global clients in around the world</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	
								<p>Prowesys’s outsourcing services in USA help clients to streamline their IT functions & improve their bottom-line through faster time to market & Significant Cost Advantage. We help you set up an Offshore Development Centre under our company, augment skilled / highly qualified IT Professionals as per your requirements. Our well-defined processes will facilitate you in sustaining the same quality, effectiveness and responsiveness without hiring expensive manpower. With our unique offshore business model, we serve global clients in around the world.</p>

<p>•	Offshore Outsourcing<br>
•	Outsourced Product Development<br>
•	Delivery Methodology<br>
•	Engagement Models<br>
•	Staff Augmentation Services<br>
•	Offshore Development Centre<br>

</p>

<p>We are engaged in providing high quality, reliable, prompt, cost-effective and client-centric Outsourcing Services to our clients.</p>
<p>
o	Boost your business bottom line with our unique OPD engagement model<br>
o	Set up your own Offshore Development Centre (ODC) to start with, will run under our company<br>
o	Augment skilled and highly qualified IT Professionals as per your requirements<br>
o	Our Offshore Outsourcing Model has been successfully deployed in more than 10 companies<br>

</p>
<div class="image">
                                	<img src="images/out.jpg" alt="">
                                </div>
                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                          <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Expertise Services</h2></div>
							<div class="widget-content">
                            	<ul>
                                	  <li><a href="{{ url ('/enterprise_mobility') }}">Enterprise Mobility</a></li>
										<li><a href="{{ url ('/enterprise_app_management') }}">Enterprise Application Management</a></li>
										<li><a href="{{ url ('/application_maintenace') }}">Application Maintenance</a></li>
										<li><a href="{{ url ('/mechanical_services') }}">Mechanical Services</a></li>
										<li><a href="{{ url ('/digital_marketing') }}">Digital Marketing</a></li>
										<li><a href="{{ url ('/testing_services') }}">Testing Services</a></li>
										<li><a href="{{ url ('/erp_practice') }}">ERP Practice</a></li>
										<li><a href="{{ url ('/outsourcing') }}">Outsourcing</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection