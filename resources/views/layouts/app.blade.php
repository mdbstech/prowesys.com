<!DOCTYPE html>
<html>


<head>
<meta charset="utf-8">
<title>Prowesys Inc.</title>
<!-- Stylesheets -->
<link href="{{ asset ('css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/revolution/css/settings.css') }}" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="{{ asset('plugins/revolution/css/layers.css') }}" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="{{ asset('plugins/revolution/css/navigation.css') }}" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->

<link href="{{ asset ('css/style.css') }}" rel="stylesheet">
<link href="{{ asset ('css/responsive.css') }}" rel="stylesheet">

<link rel="shortcut icon" href="{{ asset ('images/fav.png') }}" type="image/x-icon">
<link rel="icon" href="{{ asset ('images/fav.png') }}" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js') }}"></script><![endif]-->
<!--[if lt IE 9]><script src="{{ asset ('js/respond.js') }}"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header -->
    <header class="main-header header-style-two">

            <!-- Header Top -->
        <div class="header-top">
            <div class="auto-container">
                <div class="clearfix">
                    
                    <!--Top Left-->
                    <div class="top-left">
                        <ul class="links clearfix">
                           
                            <li></li>
                            <li>Digital Transformation</li>
                                          
                        </ul>
                    </div>
                    
                    <!--Top Right-->
                    <div class="top-right clearfix">
                    
                              <!--Top Left-->
                          <ul class="links clearfix">
                                <li>Email: info@prowesys.com</li>
                            </ul>
                    </div>
                    
                </div>
                
            </div>
        </div>
        <!-- Header Top End -->
    
        <!-- Main Box -->
    	<div class="main-box">
        	<div class="auto-container">
            	<div class="outer-container clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <div class="logo"><a href="{{ url ('home') }}"><img src="{{ asset ('images/logo.png') }}" alt="" style="width: 280px;"></a></div>
                    </div>
                         
                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                    
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->    	
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="{{ url ('/about-us') }}">About Us</a></li>
                                    
                                    <li class="dropdown"><a class="pagess" href="javascript:">Expertise Services</a>
                                                <ul style="width: 320px;">
                                                    <li><a href="{{ url ('/enterprise_mobility') }}">Enterprise Mobility</a></li>
                                                    <li><a href="{{ url ('/enterprise_app_management') }}">Enterprise Application Management</a></li>
                                                    <li><a href="{{ url ('/application_maintenace') }}">Application Maintenance</a></li>
                                                    <li><a href="{{ url ('/mechanical_services') }}">Mechanical Services</a></li>
                                                    <li><a href="{{ url ('/digital_marketing') }}">Digital Marketing</a></li>
                                                    <li><a href="{{ url ('/testing_services') }}">Testing Services</a></li>
                                                    <li><a href="{{ url ('/erp_practice') }}">ERP Practice</a></li>
                                                    <li><a href="{{ url ('/outsourcing') }}">Outsourcing</a></li>
                                                </ul>
                                            </li>
                                       <!--      <li class="dropdown"><a href="javascript:">Technology</a>
                                                <ul>
                                                    <li><a href="{{ url ('/technologies') }}">Overview</a></li>
                                                    <li><a href="{{ url ('/oracle') }}">Oracle</a></li>
                                                    <li><a href="{{ url ('/opensource') }}">Opensource</a></li>
                                                    <li><a href="{{ url ('/sas') }}">SAS</a></li>

                                                </ul>
                                            </li> -->
                                            
                                            <!-- <li><a href="{{ url ('/industry') }}">Industry</a></li> -->
                                            
                                            <li class="dropdown"><a href="javascript:">Innovations</a>
                                                <ul>
                                                    <li><a href="{{ url ('/iot_development') }}">IoT Development</a></li>
                                                    <li><a href="{{ url ('/business_analytics') }}">Business Analytics</a></li>
                                                    <li><a href="{{ url ('/business_intelligence') }}">Business Intelligence</a></li>
                                                    <li><a href="{{ url ('/bigdata') }}">BigData</a></li>
                                                    <li><a href="{{ url ('/data_warehousing') }}">Data Warehousing</a></li>
                                                    <li><a href="{{ url ('/devops_aws') }}">DevOps & AWS</a></li>
                                                </ul>
                                            </li>

                                            
                                            <li class="dropdown"><a href="{{ url ('/careers') }}">Careers</a>

                                                    <ul>
                                                    <li><a href="{{ url ('/jobs') }}">Search Jobs</a></li>
                                                    
                                                    </ul>

                                            </li>

                                         


                                            <li><a href="{{ url ('/contact-us') }}">Contact Us</a></li>
                                 </ul>
                            </div>
                        </nav>
                     
                        
                    </div>
                    <!--Nav Outer End-->
                    
            	</div>    
            </div>
        </div>
    
    </header>
    <!--End Main Header -->


    @yield('content')


     <!--Main Footer-->
    <footer class="main-footer">
        <div class="auto-container">
            <!--Upper Box-->
           <br>
            
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                
                    <!--Network Column-->
                    <div class="column links-column col-md-3 col-sm-6 col-xs-12">
                        <ul class="links-footer">
                            <li><h3>About Company</h3></li>
                            <li><a href="{{ url ('/about-us') }}">About Us</a></li>
                            <li><a href="{{ url ('/industry') }}">Industry</a></li>
                            <li><a href="{{ url ('/careers') }}">Careers</a></li>
                            <li><a href="{{ url ('/contact-us') }}">Contact Us</a></li>
                        </ul>
                    </div>
                    
                    <!--Links Column-->
                    <div class="column links-column col-md-3 col-sm-6 col-xs-12">
                        <ul class="links-footer">
                            <li><h3>Expertise Services</h3></li>
                            <li><a href="{{ url ('/enterprise_mobility') }}">Enterprise Mobility</a></li>
                            <li><a href="{{ url ('/enterprise_app_management') }}">Enterprise Application Management</a></li>
                            <li><a href="{{ url ('/application_maintenace') }}">Application Maintenance</a></li>
                            <li><a href="{{ url ('/mechanical_services') }}">Mechanical Services</a></li>
                            <li><a href="{{ url ('/digital_marketing') }}">Digital Marketing</a></li>
                            <li><a href="{{ url ('/testing_services') }}">Testing Services</a></li>
                            <li><a href="{{ url ('/erp_practice') }}">ERP Practice</a></li>
                            <li><a href="{{ url ('/outsourcing') }}">Outsourcing</a></li>
                        </ul>
                    </div>
                    
                    <!--Appointment Column-->
                    <div class="column links-column col-md-3 col-sm-6 col-xs-12">
                        <ul class="links-footer">
                            <li><h3>Technology</h3></li>
                           <li><a href="{{ url ('/technologies') }}">Overview</a></li>
                            <li><a href="{{ url ('/oracle') }}">Oracle</a></li>
                            <li><a href="{{ url ('/opensource') }}">Opensource</a></li>
                            <li><a href="{{ url ('/sas') }}">SAS</a></li>
                        </ul>
                    </div>
                    
                    <div class="column links-column col-md-3 col-sm-6 col-xs-12">
                        <ul class="links-footer">
                            <li><h3>Innovations</h3></li>
                            <li><a href="{{ url ('/iot_development') }}">IoT Development</a></li>
                            <li><a href="{{ url ('/business_analytics') }}">Business Analytics</a></li>
                            <li><a href="{{ url ('/business_intelligence') }}">Business Intelligence</a></li>
                            <li><a href="{{ url ('/bigdata') }}">BigData</a></li>
                            <li><a href="{{ url ('/data_warehousing') }}">Data Warehousing</a></li>
                            <li><a href="{{ url ('/devops_aws') }}">DevOps & AWS</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
        
        <!--Footer Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="row clearfix">
                    
                    <!--Column-->
                    <div class="column col-md-6 col-sm-12 col-xs-12">
                        <div class="copyright">Copyright &copy; 2020 <a href="javascript:">Prowesys Inc.</a> All rights reserved.</div>
                    </div>
                    
                    <!--Social Column-->
                    <div class="social-column col-md-6 col-sm-12 col-xs-12">
                        <ul class="social-icon-one">
                            <li><a href="javascript:"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="javascript:"><span class="fa fa-twitter"></span></a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
        
    </footer>
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>

<script src="{{ asset ('js/jquery.js') }}"></script> 
<!--Revolution Slider-->
<script src="{{ asset('plugins/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('plugins/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
<script src="{{ asset ('js/main-slider-script.js') }}"></script>

<script src="{{ asset ('js/bootstrap.min.js') }}"></script>
<script src="{{ asset ('js/jquery.fancybox.pack.js') }}"></script>
<script src="{{ asset ('js/owl.js') }}"></script>
<script src="{{ asset ('js/wow.js') }}"></script>
<script src="{{ asset ('js/appear.js') }}"></script>
<script src="{{ asset ('js/jquery-ui.js') }}"></script>
<script src="{{ asset ('js/script.js') }}"></script>


<!--Google Map APi Key-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHzPSV2jshbjI8fqnC_C4L08ffnj5EN3A"></script>
<script src="{{ asset ('js/gmaps.js') }}"></script>
<script src="{{ asset ('js/map-script-2.js') }}"></script>
<!--End Google Map APi-->

</body>


</html>