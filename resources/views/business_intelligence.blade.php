@extends('layouts.app')

@section('content')

  <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/buss.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Innovations</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Business Intelligence</div>
                                <h2>We provide a new age of digital transformation solution</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<div class="wpb_wrapper">
			<p>In today’s digital world, Business intelligence (BI) services help many organizations transform their businesses by organizing, updating and utilizing data in more dynamic and fastest way by identifying opportunities and to implement more quicker decisions through BI services.</p>

<p>Prowesys  provides a new age of digital transformation solution for many enterprises to bring the significant change in their existing business operations and systems to enhance better performance and profitable growth.</p>

<h3>BI Services and Solutions</h3>

<p>
To create a strategic roadmap that defines BI health check and revitalization<br>
To provide tailor insights for your business that can help to maximize new revenue generating opportunities<br>
To improvise a business operation efficiently and visibility across the organization<br>
To implement executive dashboards, interactive reporting, budgeting and rolling forecasts<br>
To implement real-time reporting with analytical alert<br>
To optimize Key Performance Indicators (KPIs)<br>
To enable faster problem-solving and decision making strategies across all business operations<br>
Upgrades, implementation, and maintenance of technologies, resources, tools, and software(s) used to make your business operations more agile.<br>
</p>

<p>Prowesys  are exceptional in providing business consulting and technology implementation experts to every client. Our BI services help you to transform great insights of reliable and real-time information across all levels of organizations which are easier to understand.</p>

Customize BI dashboard services for overall performance and scorecard
Focus on cost-effective approach for better business productivity
BI performance management tools for predictive analysis and to make tasks and reporting easier
BI architecture and design services to cater all sizes of businesses and companies
We have a set of BI team who provide comprehensive BI solutions that are easy-to understand in critical business metrics and help them to reinvent business making strategies by embracing technologies, tools, and application that enables strategic decision-making solutions.</p>

		</div>

<div class="image">
                                	<!-- <img src="images/business.jpg" alt=""> -->
                                </div> 

                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                           <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Innovations</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/iot_development') }}">IoT Development</a></li>
									<li><a href="{{ url ('/business_analytics') }}">Business Analytics</a></li>
									<li><a href="{{ url ('/business_intelligence') }}">Business Intelligence</a></li>
									<li><a href="{{ url ('/bigdata') }}">BigData</a></li>
									<li><a href="{{ url ('/data_warehousing') }}">Data Warehousing</a></li>
									<li><a href="{{ url ('/devops_aws') }}">DevOps & AWS</a></li>
                                </ul>
                            </div>
                        </div>
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection