@extends('layouts.app')

@section('content')

   <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/ser6.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Expertise Services</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Digital Marketing</div>
                                <h2>We build customized, highly-engaging digital marketing solutions.</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">

								<p>We provide comprehensive online marketing, social media marketing, mobile marketing, and email marketing solutions to leading marketing agencies and enterprises worldwide. Leveraging our hands-on industry experience and expertise in underlying technologies, we help you build customized, highly-engaging digital marketing solutions.</p>

<p>Changing trends in online shopping coupled with the unprecedented challenge for companies to influence consumer buying decisions are motivating them to shift their offline budgets to digital marketing. Companies are embracing all forms of digital marketing – these include online / website marketing, social media marketing, mobile marketing, and email marketing. However, all companies are not adequately agile to take advantage of the latest advancements in digital technology while trying to meet rapidly changing consumer requirements and ever-reducing time-to-market.</p>

<p>We have worked with leading marketing agencies and enterprises on multiple campaigns worldwide. We thereby understand your business, your pain points, and what is necessary to maximize the efficiency of your digital marketing strategies. With proven experience in the digital industry and deep expertise in emerging technologies, we help you successfully manage your digital marketing initiatives and create positive consumer experiences.</p>

<h3>OUR DIGITAL MARKETING SERVICES INCLUDE:</h3>
<ul>
<li>Online Marketing</li>
<li>Social Media Marketing</li>
<li>Mobile Marketing</li>
<li>Email Marketing</li>
</ul><br>

<h3>TOOLS & TECHNOLOGIES:</h3>
<ul>
<li>FLASH / FLEX / LASZLO / SILVERLIGHT</li>
<li>MOSS/ Sitecore / Tridion / Joomla / Rhythmyx</li>
<li>.NET / JAVA / PERL / PHP</li>
<li>HTML / SML / DHTML / xHTML / CSS / AJAX / Java Script / ASP.NET</li>
</ul>
<div class="image">
                                	<img src="images/dm.jpg" alt="">
                                </div> 
                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                            
                        <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Expertise Services</h2></div>
							<div class="widget-content">
                            	<ul>
                                	  <li><a href="{{ url ('/enterprise_mobility') }}">Enterprise Mobility</a></li>
										<li><a href="{{ url ('/enterprise_app_management') }}">Enterprise Application Management</a></li>
										<li><a href="{{ url ('/application_maintenace') }}">Application Maintenance</a></li>
										<li><a href="{{ url ('/mechanical_services') }}">Mechanical Services</a></li>
										<li><a href="{{ url ('/digital_marketing') }}">Digital Marketing</a></li>
										<li><a href="{{ url ('/testing_services') }}">Testing Services</a></li>
										<li><a href="{{ url ('/erp_practice') }}">ERP Practice</a></li>
										<li><a href="{{ url ('/outsourcing') }}">Outsourcing</a></li>
                                </ul>
                            </div>
                        </div>
                    
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection