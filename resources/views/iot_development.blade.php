@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/ino1.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Innovations</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">IoT Application Development</div>
                                <h2>We create an intelligent farming system that combines IoT technology</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<p>Internet of Things provides numerous new opportunities from improving and simplifying our everyday life to generating efficiencies and value for business operations. Smart homes, health monitoring, use of sensors and analytics to make smarter decisions all use the power of connected devices. The potential to transform the way we work is right here and starting with Internet of Things application development can turn your ideas into the next big thing.</p>
								<p>For 15+ years we have been building business critical applications for our clients. Applications that improve their operational efficiency, help connect their services and customers and introduce innovation to their new products. We already have extensive experience in all the critical components of an IoT platform – hardware (through our development partner), cloud software, analytics and web & mobile application interfaces – so it is only natural we expanded to offering IoT solutions.</p>

<p>All our developers have full stack technical capabilities and an Agile mindset to help you explore all the possibilities for your IoT applications. Our development teams have been working on several exciting projects in the past years, bringing the “smart” to life in the applications.</p>

<div class="image">
                                	<img src="images/iot.png" alt="">
                                </div> 

                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                        <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Innovations</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/iot_development') }}">IoT Development</a></li>
									<li><a href="{{ url ('/business_analytics') }}">Business Analytics</a></li>
									<li><a href="{{ url ('/business_intelligence') }}">Business Intelligence</a></li>
									<li><a href="{{ url ('/bigdata') }}">BigData</a></li>
									<li><a href="{{ url ('/data_warehousing') }}">Data Warehousing</a></li>
									<li><a href="{{ url ('/devops_aws') }}">DevOps & AWS</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection