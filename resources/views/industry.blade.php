@extends('layouts.app')

@section('content')

<!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/background/9.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Industry</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Industry Expertise</div>
                                <h2>We provide extensive services to our clients</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<h3>Domain Expertise</h3>
Domain expertise across many verticals has been the key capability of Prowesys  to provide extensive services to its clients

Our teams comprise of professionals with many years of experience in their respective domains and bring together versatile skill sets. By combining technology expertise and operational excellence with domain capabilities, we are able to provide solutions best suited to our clients.

We have helped companies make the most out of their websites, email campaigns, off-site ad-campaigns, viral marketing initiatives, by identifying customers from the cloud of visitors.

Prowesys  HAS DISTINCTION IN WORKING FOR DOMAINS SUCH AS:
Banking and Finance	Hospitality
Education	Mining
E- Governance	Real Estate
Finance	Retail
Healthcare	 

<ul class="accordion-box style-two">
                            
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>Banking and Financial Services.</div>
                                <div class="acc-content current">
                                    <div class="content">
                                        <div class="text">
										<div>

<p>With its extensive experience and in-depth understanding of the banking and the financial services industry, Prowesys    has developed innovative, cost-effective solutions that not only take on today’s challenges but also ensure that its clients have a competitive edge over others. </p>

<p>One of the main cornerstones of our approach is the business process view on the functionality and use of information systems. This means that the value of an information system is determined in operative use as a function of its support for existing processes and practices.</p>

<h3>Our solutions that cover the entire gamut of Banking and Financial Service industry include:</h3>
<ul>
 	<li>Internet banking </li>
 	<li>Payment Integration </li>
 	<li>Broking/Trading Solutions</li> 
 	<li>Insurance Software </li>
 	<li>Claims Processing </li>
 	<li>Portfolio Planning and Management </li>
 	<li>Content Management </li>
</ul>	

<p>Our approach towards optimizing operational efficiencies is promoted through robust architecture and development methodologies, automated, high-coverage testing and proactive support for regulatory compliance.</p>


  </div>
										</div>
                                    </div>
                                </div>
                            </li>
    
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>Education</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">
										<div>
<h3>Helping educators achieve better results with technology. </h3>

<p>For years Prowesys    has worked with schools, colleges and Universities around the world to achieve a vision of better educational results through technology. From the start, our focus has been on providing educational software solutions that empower the teaching process. It is our belief that through the appropriate use of technology, students can become more engaged in the subject matter, learn faster, and benefit from a higher retention rate.</p>

<p>Good management of schools and training services is critical to the success of the overall learning system.</p>
<h3>Topics we cover include:</h3>
<p> 	<strong>ELearning Support &amp; Services</strong> – eLearning planning guide, internet and email acceptable use policy, Virtual Campus, Technical Support to Schools Program, telematics &amp; videoconferencing.</p>

<p> 	<strong>School Administration</strong> – Accreditation &amp; registration, information management, school records, school community information, student management, school policy, Schools Reference Guide, student data collection &amp; reports.</p>

<p> 	<strong>Financial Management</strong> – School financial reporting, finance policy &amp; procedures, finance resources, funding, proformas &amp; worksheets, taxation , vocational education &amp; training funding.</p>
<p> 	<strong>School Operations</strong> – School services, student administration, student health and safety, financial administration, school communications and marketing.</p>
<p> 	<strong>Support &amp; Services</strong> – Administrative &amp; finance system, document systems – Edu Library, security policy, intranet – IT updates, local area networks, software.</p>

<p>We understand the education industry has specific priorities in its software architecture. Managing the time in building the most effective solution, minimizing the cost of operation and support, are the key of a reliable system that we always oblige. We have the know-how that can be used to minimize time-to-market of such applications, minimize cost of development and support. </p>

<p>Creating the difference by implementing software does not just work without business understanding, we can help integrating applications, creating new modules to the existing system, migration of data, legacy system to advanced architecture system and more. </p>

<p>Our business experts include professors from the world class universities. Prowesys    can help you build applications catering to your requirements.</p>

  </div>
										</div>
                                    </div>
                                </div>
                            </li>
                            
                            <!--Block-->
                            <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>E- Governance</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">
										<div>
<p>Information Technology (IT) is playing a significant role in enabling government to deliver a wide range of services more resourcefully to the citizens. Prowesys    partners with government organizations and departments and assist them use IT to bring in greater transparency and flexibility in the operations.</p>

<p>Our services help government agencies improve operational efficiencies, reduce project costs, speed up implementation cycles, and become more citizen-friendly.</p>
<h3>Our e-Governance Services</h3>
<p> 	IT Project Management and Consulting <br>
 	Business Planning and IT Strategy Consulting <br>
 	Security Consulting <br>
 	Network and IT Infrastructure Consulting <br>
 	Capacity Building and Training </p>

<h3>Solutions </h3>
<p> 	Customized Software Development <br>
 	System Integration <br>
 	Design, Implementation and Maintenance of Data Centers <br>
 	IT Infrastructure Services</p>

  </div>
</div>
                                    </div>
                                </div>
                            </li>
							
							 <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>Healthcare</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">
										
										<div>
    <p>Prowesys  is exclusively focused in providing Healthcare Solutions. Our in-depth understanding of this industry enables us to provide innovative end-to-end solutions to our clients.</p>

<p>We employ a global delivery model to rapidly deliver high quality, cost-effective solutions. By basing Support and Account Management we provide our clients with high quality customer service.</p>

<p>Our Team Strives to provide you with custom software, Web solutions and Search Engine Optimization that will empower you to remain a head of your competitors by continuously improving your information technology based business solutions</p>
<p>Centrica’ provides the following services to Healthcare Organizations.</p>
<ul>
<li>Accounts Management</li>
 	<li>Electronic Medical Records – EMR</li>
<li> 	Electronic Health Records – EHR</li>
 	<li>Revenue cycle management</li>
 	<li>Practice Management</li>
 	<li>Reporting</li>
 	<li>Web Portals</li>
 	<li>Web Marketing</li>
	</ul>

  </div>
										
										</div>
                                    </div>
                                </div>
                            </li>
							
							 <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>Hospitality</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">
										<div>
    <p>Prowesys    hospitality software solutions will help you improve profitability and better service to your guests. Our suite of software solutions—including customer relationship management, financial management, performance management, leisure management, and human resource management—allow you to </p>

<p> 	Integrate business applications — Share information effectively across your enterprise.</p>
<p> 	Consolidate information — create a centralized profile for each guest to efficiently track preferences, simplify reservations, book appointments, and deliver personalized service across multiple properties.</p>
 <p>	Increase knowledge about your customers — Access historical data to identify your most profitable guests and pinpoint how to provide guest-centric service that will build loyalty and increase the value of each stay.</p>
<p> 	Enhance back-office business processes — Automate and improve effectiveness of finance, business planning and reporting, HR, and other functions to facilitate efficient reporting and analysis, with a focus on bottom-line profitability. </p>
 
<h3>Prowesys    Web Marketing Services include</h3>
<ul>
 	<li>Email Marketing &amp; Newsletters</li>
 	<li>Web ads</li>
 	<li>Search Engine Optimization</li>
 	<li>Online PR (Public Relations) Services</li>
 	<li>Search Engine Marketing</li>
 	<li>Web Analytics Tool</li>
 	<li>Bulk SMS campaign</li>
 	<li>Online Survey tool</li>
</ul>	

<p>When you choose our hospitality software &amp; marketing solutions, you’re backed by a reliable technology and marketing partner. We stand behind our solutions, providing you with excellent support, service, and a strong commitment to helping you achieve your business goals. </p>

<p></p>
  </div>
										</div>
                                    </div>
                                </div>
                            </li>
							
							 <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>Mining</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">
										<p>We provide services for the mining industry with major specialisation in Software application areas. We have extensive planning experience in numerous projects. Using the ERP tools and techniques developed over the years, we can improve your mining plans and optimise your mining strategy.</p>

<p>Regardless of the size of the project, effective project management can be achieved by efficient utilisation of available resources, technical processes and tools in a dynamic and communicative way.</p>

<p>Our sound understanding of the cyclic relationship between planning variables and processes can provide you with the solutions you need to achieve your objectives.</p>

<p>Software tools available today largely facilitate the iterative planning cycle by introducing shortcuts and making possible detailed evaluations with additional inputs at various stages of a project.</p>
										</div>
                                    </div>
                                </div>
                            </li>
							
							 <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>Real Estate</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">
										<div class="">
    <p>Prowesys    main focus of expertise has been primarily focused on Reality and Infrastructure companies. The secret to our success is our ability to provide unparalleled exposure in extremely competitive markets. </p>

<p>Prowesys    Internet Marketing specializes within the Reality industry, in providing solutions that are successful, innovative, professional, and of high quality. Respect is earned through leadership and a commitment to service, quality, and results. </p>
<ul>
 	<li>We deliver a high quality service which produces results</li> 
 	<li>We know that our customers’ success ensures our success </li>
 	<li>We recognize that quality customer service please customers </li>
 	<li>We seek customers who share our vision and wish to work in a partnership </li>
</ul>
 
<h3>Services We Offer For Real Estate Industry </h3>
<ul>
 	<li>Professional Web design </li>
 	<li>Content Management System</li>
 	<li>Customer Login Portal</li>
 	<li>Live Chatting service</li>
 	<li>ecommerce </li>
 	<li>Lead Management System</li>
 	<li>HRMS</li>
 	<li>ERP</li>
</ul>	

<h3>Prowesys    Web Marketing Services include</h3>
<ul> 	
	<li>Email Marketing &amp; Newsletters</li>
 	<li>Web ads</li>
 	<li>Search Engine Optimization</li>
 	<li>Online PR (Public Relations) Services</li>
 	<li>Search Engine Marketing</li>
 	<li>Web Analytics Tool</li>
 	<li>Bulk SMS campaign</li>
 	<li>Maintenance and security</li>
 	<li>Online Survey</li>
</ul>
 
<h3>What Makes Us Unique in Real Estate Sector?</h3>
<ul>
<li>We have a team dedicated to research and development to stay on top of what is going on in Realty sector to keep your site fresh and ahead of your competition. </li>
<li>We are focused on internet marketing within the Realty niche, which gives you the advantage of our experience. </li>
<li>We value our customers and strive to give excellent service. We enjoy our jobs and it shows! </li>
</ul>
  </div>
										</div>
                                    </div>
                                </div>
                            </li>
							
							 <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div>Retail</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">
										<div class="">
    <p>Our retail consulting experience provides valuable inputs into retail processes, best and successful practices and pin points potential problems to our clients, which include:-</p>
<ul>
<li>Business needs </li>
<li>Procedures </li>
<li>Software evaluation </li>
<li>Selection </li>
<li>Planning </li>
<li>Analysis </li>
</ul>	
<h3>Why we are Successful?</h3>
<p>Prowesys operates on creating meaningful, long-term relationships with our clients. We aspire to provide the highest level of quality and services, which provides the incentive for clients to return. It’s this relationship that works out best for any organization </p>

  </div>
										</div>
                                    </div>
                                </div>
                            </li>
                            
                        </ul>
                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                        <!-- Category List Widget -->
                       <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>About Company</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/about-us') }}">About Us</a></li>
									<li><a href="{{ url ('/industry') }}">Industry</a></li>
									<li><a href="{{ url ('/careers') }}">Careers</a></li>
									<li><a href="{{ url ('/contact-us') }}">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection
