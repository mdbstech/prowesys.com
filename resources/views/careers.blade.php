@extends('layouts.app')

@section('content')

    <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/car.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Careers</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Careers</div>
                                <h2>We recruit best people in the industry and to help them grow and create value for themselves</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<p>Looking for the best and brightest professionals committed to creating, developing and implementing innovative technology and business solutions for our customers.</p>

<h3>Current Openings</h3>
<p>Prowesys  offers a wide range of technologies and platforms for great learning environment, providing opportunities in various fields and immense growth potential. At Prowesys  you can find challenging opportunities in an environment that recognizes and rewards exceptional performance. We attract some of the finest talents and always looking to recruit best people in the industry and to help them grow and create value for themselves and to make the most of your time.</p>

<p>If you are interested in being a part of one of the Prowesys  then join us for an opportunity that will advance you in every way.</p>

<p>The resume can be sent to info@prowesys.com </p>

<h3>EMPLOYEE REFERRAL PROGRAM</h3>
<p>Find out more about how you can refer someone to join Prowesys  and get rewarded. We welcome referrals of any candidates who would make an outstanding addition to the Prowesys  team. We are looking for professionals committed to creating, developing, and implementing innovative technology and business solutions for our customers.</p>

<p>Prowesys  will pay a referral bonus to employees/consultants for candidate referrals for contract or full-time positions. The amount of the bonus will be based on the candidate’s qualifications, credentials, level of experience, and market demand.</p>

<p>The resume can be sent to info@prowesys.com</p>

                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                        <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>About Company</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/about-us') }}">About Us</a></li>
									<li><a href="{{ url ('/industry') }}">Industry</a></li>
									<li><a href="{{ url ('/careers') }}">Careers</a></li>
									<li><a href="{{ url ('/contact-us') }}">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection
