@extends('layouts.app')

@section('content')


<!--Main Slider-->
    <section class="main-slider">
    	<div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
                <ul>
                	
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-1.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-1.jpg"> 
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['950','750','750','550']"
                    data-whitespace="normal"
                    data-hoffset="['0','0','0','0']"
                    data-voffset="['-80','-120','-120','-120']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<div class="title"><span>We are</span> Prowesys</div>
                    </div>
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['950','750','750','550']"
                    data-whitespace="normal"
                    data-hoffset="['0','0','0','0']"
                    data-voffset="['50','0','-10','-20']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<h2 class="text-center">Business Analytics <br> Intelligence</h2>
                    </div>
                    
                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['560','700','700','550']"
                    data-whitespace="normal"
                    data-hoffset="['0','0','0','0']"
                    data-voffset="['180','120','100','100']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<div class="btns-box text-center">
                    		<a href="{{ url ('/about-us') }}" class="theme-btn btn-style-one">About Our Company</a>
                        </div>
                    </div>
                    
                    </li>
                    
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1698" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-2.jpg"> 
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['950','750','750','550']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['0','-90','-60','-60']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<h2 class="alternate">Our <span>Mission</span> is <br> make the world to <br> more capable.</h2>
                    </div>
                    
                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['560','700','700','550']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['180','90','80','60']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<div class="btns-box">
                    		<a href="{{ url ('/technologies') }}" class="theme-btn btn-style-one">Our Technology</a>
                        </div>
                    </div>
                    
                    </li>
                    
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-3.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-3.jpg"> 
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['950','750','750','550']"
                    data-whitespace="normal"
                    data-hoffset="['0','0','0','0']"
                    data-voffset="['-80','-120','-120','-100']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<div class="title">Enterprise Applications </div>
                    </div>
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['950','750','750','550']"
                    data-whitespace="normal"
                    data-hoffset="['0','0','0','0']"
                    data-voffset="['50','0','-10','-15']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<h2 class="text-center">We providing  <br> end to end </h2>
                    </div>
                    
                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['560','700','700','550']"
                    data-whitespace="normal"
                    data-hoffset="['0','0','0','0']"
                    data-voffset="['180','120','100','85']"
                    data-x="['center','center','center','center']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<div class="btns-box text-center">
                    		<a href="{{ url ('/enterprise_app_management') }}" class="theme-btn btn-style-one">Our Services</a>
                        </div>
                    </div>
                    
                    </li>
                    
                </ul>
            </div>
        </div>
       
    </section>
    <!--End Main Slider-->
     <!--Fluid Section One-->
    <section class="fluid-section-one">
    	<div class="outer-container clearfix">
        	<!--Image Column-->
            <div class="image-column" style="background-image:url(images/resource/image-1.png);">
            	<figure class="image-box"><img src="images/resource/image-1.png" alt=""></figure>
            </div>
        	<!--Content Column-->
            <div class="content-column">
            	<div class="inner-column">
					<!--Sec Title-->
                    <div class="sec-title">
                    	<div class="title">Prowesys Unique Info</div>
                        <h2>About Company</h2>
                    </div>
                    <div class="text">
                    	<p>Prowesys quickly defines its stand as one of the leading Software Development and Technical Consulting Services in USA. </p>
                        <h3>Prowesys successfully thrives in a  <br><span>growing and competitive</span><br> hi-tech market as a top ranked software development company.</h3>
						   
                        <p>Our experience, expertise, best-equipped facility, professional management and commitment have been bringing our customers utmost satisfaction and in return rewarded us with rapid and sustainable growths over the years.</p>
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
    <!--End Fluid Section One-->
    <!--Services Section-->
    
    <!--End Services Section-->
    <section class="project-section-two">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title centered light">
            	<div class="title">Our Expertise Services</div>
                <h4>As enterprise solution providers on technical capabilities</h4>
            </div>
        </div>
        
        <div class="outer-container">
        	<div class="four-item-carousel owl-carousel owl-theme">
            	
                <!--Project Block Two-->
                <div class="project-block-two">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/service_3.jpg" alt="" />
                            <div class="overlay-box">
                            	<div class="overlay-inner">
                                	<div class="category">Enterprise Mobility</div>
                                    <h2>We providing end to end Mobile Application development solutions</h2>
                                	<a href="{{ url ('/enterprise_mobility') }}" class="go"><span class="icon flaticon-next-3"></span>Go through</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Project Block Two-->
                <div class="project-block-two">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/service_8.jpg" alt="" />
                            <div class="overlay-box">
                            	<div class="overlay-inner">
                                	<div class="category">Enterprise Application Management</div>
                                    <h2>We at Prowesys help businesses to execute their daily business activities perfectly</h2>
                                	<a href="{{ url ('/enterprise_app_management') }}" class="go"><span class="icon flaticon-next-3"></span>Go through</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Project Block Two-->
                <div class="project-block-two">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/service_2.jpg" alt="" />
                            <div class="overlay-box">
                            	<div class="overlay-inner">
                                	<div class="category">Application Maintenance</div>
                                    <h2>Data Conversion, Migration and Maintenance</h2>
                                	<a href="{{ url ('/application_maintenace') }}" class="go"><span class="icon flaticon-next-3"></span>Go through</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Project Block Two-->
                <div class="project-block-two">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/service-5.jpg" alt="" />
                            <div class="overlay-box">
                            	<div class="overlay-inner">
                                	<div class="category">Mechanical Services</div>
                                    <h2>We strong performance, productivity and retention</h2>
                                	<a href="{{ url ('/mechanical_services') }}" class="go"><span class="icon flaticon-next-3"></span>Go through</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Project Block Two-->
                <div class="project-block-two">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/service_5.jpg" alt="" />
                            <div class="overlay-box">
                            	<div class="overlay-inner">
                                	<div class="category">Digital Marketing</div>
                                    <h2>We build customized, highly-engaging digital marketing solutions</h2>
                                	<a href="{{ url ('/digital_marketing') }}" class="go"><span class="icon flaticon-next-3"></span>Go through</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Project Block Two-->
                <div class="project-block-two">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/service-3.jpg" alt="" />
                            <div class="overlay-box">
                            	<div class="overlay-inner">
                                	<div class="category">Testing Services</div>
                                    <h2>Our goal is to optimize seamless experience by identifying bugs</h2>
                                	<a href="{{ url ('/testing_services') }}" class="go"><span class="icon flaticon-next-3"></span>Go through</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Project Block Two-->
                <div class="project-block-two">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/project-21.jpg" alt="" />
                            <div class="overlay-box">
                            	<div class="overlay-inner">
                                	<div class="category">ERP Practice</div>
                                    <h2>For total planning of your enterprise resource needs</h2>
                                	<a href="{{ url ('/erp_practice') }}" class="go"><span class="icon flaticon-next-3"></span>Go through</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Project Block Two-->
                <div class="project-block-two">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/service-7.jpg" alt="" />
                            <div class="overlay-box">
                            	<div class="overlay-inner">
                                	<div class="category">Outsourcing</div>
                                    <h2>we serve global clients in around the world</h2>
                                	<a href="{{ url ('/outsourcing') }}" class="go"><span class="icon flaticon-next-3"></span>Go through</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </section>
    
   <section class="services-section-two">
    	<div class="auto-container">
        	<div class="outer-container"><br><h3 align="center">Why Prowesys?</h3>
                <div class="clearfix">
                    
                    <!--Services Block three-->
                    <div class="services-block-three col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="content">
                                <div class="icon-box">
                                    <span class="icon flaticon-bullseye"></span>
                                </div>
                                <h2><a href="#">Commitment</a></h2>
                                <div class="title">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                            </div>
                        </div>
                    </div>
                    
                    <!--Services Block three-->
                    <div class="services-block-three col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="content">
                                <div class="icon-box">
                                    <span class="icon flaticon-manager"></span>
                                </div>
                                <h2><a href="#">Team Work</a></h2>
                                <div class="title">We recognize that diversity is a source of strength and listen to differing viewpoints so we can constructively solve problems.</div>
                                
                            </div>
                        </div>
                    </div>
                    
                    <!--Services Block three-->
                    <div class="services-block-three col-md-4 col-sm-12 col-xs-12">
                        <div class="inner-box">
                            <div class="content">
                                <div class="icon-box">
                                    <span class="icon flaticon-certificate"></span>
                                </div>
                                <h2><a href="#">Professionalism</a></h2>
                                <div class="title">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
  
    
    <!--Clients Section-->
    <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>
    <!--End Clients Section-->

@endsection
