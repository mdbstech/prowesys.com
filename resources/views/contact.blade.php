    @extends('layouts.app')

    @section('content')

     <!--Page Title / Style Two-->
        <section class="contact-banner-post" style="background-image:url(images/background/9.jpg)">
        	<div class="auto-container">
            	<div class="upper-box">
                	<h2><span class="icon-box flaticon-mail"></span> Contact Our Team Today! <br> and Discuss About What You Need.</h2>
                </div>
                
                <!--Contact Info Blocks-->
                <div class="contact-info-blocks">
                	<div class="row clearfix">
                    	
                        <!--Info Branch Block-->
                        <div class="info-branch-block col-md-4 col-sm-6 col-xs-12">
                        	<div class="inner-box">
                            	<h3>Contact Address</h3>
                                <div class="text"><b>800 E Campbell Rd Ste 260, <br>Richardson, TX 75081. </b><br>
    
     <a href="https://goo.gl/maps/rVw2sNmLeWEZntTK8" target="_blank"> <span class="icon flaticon-web-page-home"></span> Google Location</a>
     </div>
                            </div>
                        </div>
                        
                        <div class="quick-contact-info col-md-4 col-sm-6 col-xs-12">
                        	<div class="inner-box">
                            	<h3>Quick Contact</h3>
                                <ul class="branch-list-contact">
                                	<li><span class="icon flaticon-technology-2"></span> <b>972-9965796</b></li>
                                    <li><span class="icon flaticon-note"></span> info@prowesys.com</li>
    								<li><span class="icon flaticon-web-page-home"></span> www.prowesys.com</li>
    								<li>&nbsp;</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
               
            </div>
        </section>
        <!--End Page Title-->
        
        <!--Communicate Section-->
        <section class="communicate-section">
            <div class="auto-container">
            	<div class="row clearfix">
                	
                    <!--Content Column-->
                    <div class="content-column col-md-8 col-sm-12 col-xs-12">
                    	<div class="inner-column">
                        	<h2>Please send your resume to:</h2>
                            <div class="text">careers@prowesys.com</div>
                            <div class="row clearfix">
                            

                            	<!--Communicate Box-->
                                <div class="communicater-box col-md-6 col-sm-6 col-xs-12">
                                	<h3></h3>
                                    <div class="communicate-inner">
                                    	
                                        <div class="name">Charles Mecky</div>
                                        <ul>
                                        	<li>hr@ prowesys.com </li>
    										<li>bgcp@prowesys.com </li>
                                        </ul>
                                    </div><br>
    								<div class="communicate-inner">
                                    	
                                        <div class="name">Please send general inquires to:</div>
                                        <ul>
                                        	<li>info@prowesys.com </li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <!--Communicate Box-->
                                <div class="communicater-box col-md-6 col-sm-6 col-xs-12">
                                	<h3></h3>
                                    <div class="communicate-inner">
                                    	
                                        <div class="name">Please send Requests for Proposal (RFP) inquires to:</div>
                                        <ul>
                                        	<li>rfp@prowesys.com</li>
                                        </ul>
                                    </div><br>
    								<div class="communicate-inner">
                                    	
                                        <div class="name">Product Support</div>
                                        <ul>
                                        	<li>support@prowesys.com </li>
                                        </ul>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                    <!--Form Column-->
                    <div class="form-column col-md-4 col-sm-12 col-xs-12">
                    	<div class="inner-column">
                        	<div class="title-box">
                            	<h2>Send Your Message</h2>
                                <div class="text">Dont hesitate to send messge us, Our <br> team will help you 24/7.</div>
                            </div>
                            
                            <div class="contact-form">
    						<form action="{{ url('send') }}" method="post">
                                      {{ csrf_field() }}      
    						<!-- 	<form id="contact-form" name="frmContact" action="contact.php" method="post" class="niceform"> -->
            
    								<div class="form-group">
                                        <input type="text" id="txtName" name="txtName" placeholder="Name*" required="" />
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group">
                                        <input type="text" id="txtPhone" name="txtPhone" placeholder="Phone*" required="" />
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group">
                                        <input id="txtEmail" name="txtEmail" type="text" placeholder="Email*" required="" />
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group">
                                        <input type="text" id="txtSubject" name="txtSubject" placeholder="Required Service*" required="" />
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group">
                                        <textarea id="txtComments" name="txtComments" placeholder="Message..."></textarea>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group btn-column">
                                        <button class="theme-btn btn-style-two" type="submit" name="submit-form">Submit</button>
                                    </div>
                                       @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ $message  }}&#129321;</strong>
                            </div>
                            @endif

             </form>
                            	
                            </div>
                            <!--End Contact Form -->
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!--End Communicate Section-->
        
         <!--Map Section-->
        <section class="map-section">
            <!--Map Outer-->
            <div class="map-outer">
               

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3347.109862648548!2d-96.71315018533045!3d32.974503181197704!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864c1f2662cdd92f%3A0x1585be27b9b300a0!2s800%20E%20Campbell%20Rd%20%23260%2C%20Richardson%2C%20TX%2075081%2C%20USA!5e0!3m2!1sen!2sin!4v1588141839472!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </section>
        <!--End Map Section-->


    <!--Clients Section-->
    <section class="clients-section-two">
        <div class="auto-container">
            
        </div>
    </section>
    <!--End Clients Section-->


    @endsection
