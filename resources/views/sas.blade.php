@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/sas.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Technology</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">SAS</div>
                                <h2>We providing the highest quality of open source development services</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<p>Our motto is to provide esteemed organizations from Financial Institutions, Banks, Pharmaceutical, Bio Pharmaceutical and life sciences area with Clinical Research Staffing and Consulting Solutions across multiple areas such as.</p>

» Biostatistics & SAS Programming<br>
» Clinical Data Management<br>
» Clinical Research Associates<br>
» Scientific & Laboratory<br>
» Regulatory, Safety & Medical Writing<br>
» Market Analyst<br>
» Financial Analyst<br>
» Others<br><br>

<p>Prowesys  provide following support for their project delivery using SAS Technologies such as Base SAS, SAS/Macro, SAS/SQL, SAS Business Intelligence, SAS Data Integration etc.</p>

» Development of Customized Systems for their daily operations<br>
» Automation of Regulatory reporting<br>
» Development of Data Warehouse using SAS Data Integration<br>
» Transforming Data into more suitable form to ease out the generation of Manual reports.<br>
» Development of automated processes over SAS for generation of various alerts, reminders etc. for clients and the employees both.<br>
» Managing and publishing campaigns to particular customers<br><br>

<h3>CLINICAL & LIFE SCIENCES</h3>
<p>Prowesys  provide following support for their project delivery using SAS Technologies such as Base SAS, SAS/Macro, SAS/SQL, SAS/Report, SAS Business Intelligence, SAS Clinical Data Integration etc.</p>

» Development of various reports for CTD submissions.<br>
» Automation of table generation of Demographic Data and many more.<br>
» Clinical SAS/Statistical Programming<br>
» Clinical Data Management<br>
» Development of Reports, Listings & Graphs<br>
» Technical Support for Technical & Logical Queries<br>
» Data Integration, Mining & Date Warehousing<br>
» Customized Application Development<br>
» Full & Partial e-CTD Submissions<br><br>

<h3>ANALYTICS SOLUTION IMPLEMENTATION & DELIVERY</h3>
<p>We provide solutions using SAS Technologies to resolve their business issues and to increase profitability and ROI. Also we help businesses to ease out the routine process.</p>

<h3>We provide solutions to following industries</h3>

» Banking, Finance & Insurance<br>
» Clinical & Life Sciences<br>
» Retail<br>
» IT Companies<br>
» Oil & Gas<br>
» Power<br>
» Government & Many More.<br><br>
<p>We include following development or support to various industries to increase the profitability</p>

» Management of Campaigns for their current & Upcoming prodcuts and services<br>
» Provision of right customer segments to increase the Sales of products<br>
» Managing the daily operations by provision of customized platform<br>
» Integration of their current Data system with SAS Technologies to get the insights from the Data & much more…<br>


                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                    <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Technology</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/technologies') }}">Overview</a></li>
									<li><a href="{{ url ('/oracle') }}">Oracle</a></li>
									<li><a href="{{ url ('/opensource') }}">Opensource</a></li>
									<li><a href="{{ url ('/sas') }}">SAS</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection