@extends('layouts.app')

@section('content')

  <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/ser3.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Expertise Services</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Enterprise Application Management</div>
                                <h2>We at Prowesys help businesses to execute their daily business activities perfectly</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<p>Enterprise Application performs various business functions such as human resource, accounting, order processing, procurement etc. We at Prowesys  help businesses to execute their daily business activities perfectly, by providing them complete business applications and solutions which is appropriatefortheirbusiness.</p>

<p>Our team consists of experienced and skilled software professionals who are highly proficient in life-cycle development and have expertise in developing world class commercial applications that meet business and technical requirements. We ensure that our Enterprise Applications are robust and scalable and developed keeping in mind the needs of enterprises.</p>

<p><b>Prowesys  provides the following services under Enterprise Application Development:</b></p>

» Human Resource Management<br>
» Customer Relationship Management<br>
» Supply Chain Management<br>
» Enterprise Resource Planning<br>
» Enterprise Application Integration<br>
» Enterprise Information System<br>
» E-Business Solutions<br>
» Enterprise Content Management<br>

<div class="image">
                                	<img src="images/services-1.jpg" alt="">
                                </div>
                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                            <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Expertise Services</h2></div>
							<div class="widget-content">
                            	<ul>
                                	  <li><a href="{{ url ('/enterprise_mobility') }}">Enterprise Mobility</a></li>
										<li><a href="{{ url ('/enterprise_app_management') }}">Enterprise Application Management</a></li>
										<li><a href="{{ url ('/application_maintenace') }}">Application Maintenance</a></li>
										<li><a href="{{ url ('/mechanical_services') }}">Mechanical Services</a></li>
										<li><a href="{{ url ('/digital_marketing') }}">Digital Marketing</a></li>
										<li><a href="{{ url ('/testing_services') }}">Testing Services</a></li>
										<li><a href="{{ url ('/erp_practice') }}">ERP Practice</a></li>
										<li><a href="{{ url ('/outsourcing') }}">Outsourcing</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection