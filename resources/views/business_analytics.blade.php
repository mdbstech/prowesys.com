@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/bus.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Innovations</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Business Analytics</div>
                                <h2>We create impeccable complex business processes with simple and elegant solutions</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<div class="wpb_wrapper">
			<p>Prowesys  are committed to practice exceptional business value services for our clients with insights from data. We create impeccable complex business processes with simple and elegant solutions by leveraging our strong industry and domain expertise that are required to continue to upgrade the skills with evolving integrated business analytics.</p>
<p>We understand that there is a rapid increase in business competitive and to remain in top position, the business analytics needs to drive exceptional decision-making strategies to improvise customer engagement, strengthen optimal performance, to prevent threats and fraudulent, and capitalize on new sources of revenues. This can be achieved by adopting the right business analytics process and to deliver technologies into a new market vertical to identify the user’s competitive insights and gains as well as drive to new growth strategies to indicate augmentation of the business operations.</p>
<p>Prowesys  helps small, mid-size, and large-size business enterprises to create their own business analytics platforms with more specific tools and technologies to utilize the advanced analytics needs. We are able to develop a customized model based solution that enables the best technological considerations available in the industry.</p>

		</div>

<div class="image">
                                	<img src="images/business.jpg" alt="">
                                </div> 

                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                            <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Innovations</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/iot_development') }}">IoT Development</a></li>
									<li><a href="{{ url ('/business_analytics') }}">Business Analytics</a></li>
									<li><a href="{{ url ('/business_intelligence') }}">Business Intelligence</a></li>
									<li><a href="{{ url ('/bigdata') }}">BigData</a></li>
									<li><a href="{{ url ('/data_warehousing') }}">Data Warehousing</a></li>
									<li><a href="{{ url ('/devops_aws') }}">DevOps & AWS</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection