@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/background/9.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">About Company</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">About Prowesys</div>
                                <h2>We think and act ahead to be the most effective and efficient</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<p>Prowesys quickly defines its stand as one of the leading Software Development and Technical IT Services in USA. Our experience, expertise, best-equipped facility, professional management and commitment have been bringing our customers utmost satisfaction and in return rewarded us with rapid and sustainable growths over the years.</p>
								
<p>Being based in USA, Prowesys successfully thrives in a growing and competitive hi-tech market as a top ranked software development company, software outsourcing service provider, and cost-effective website design firm. Prowesys provides you with premium and comprehensive solutions in Offshore Outsourcing Development Service, Software Development, Custom Website Design, Ecommerce Website Design and Mobile Application Development.</p>

<p>Apart from the recognized quality services, Prowesys has always been the first choice for customers of needs in terms of prices. The USA professional software offshore outsourcing company is committed to provide international standard services for its customers at affordable rates.</p>

<p>In order to satisfy customers and pursue our own development path, our people not only possess a firm knowledge and sound understanding of the works they are involved in but also have a clear mindset towards corporate working philosophy, which can be shortly described as:</p>

<h3> Commitment</h3>
<p>We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</p>

<h3> Team-work</h3> 
<p>We recognize that we are stronger and more effective as a team than as individuals. We support an open, communicative culture in which individuals are encouraged to offer suggestions for improvement. We recognize that diversity is a source of strength and listen to differing viewpoints so we can constructively solve problems. Fostering strong teamwork allows an opportunity for each individual's suggestions to be heard, empowering our employees and encouraging necessary risk-taking. Teamwork increases our chance for success.</p>

<h3> Professionalism</h3> 
<p>We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care. At Prowesys, monthly performance appraisal is applied so that the professionalism is deeply understood and conduct on every staff.</p>

<h3> Proactiveness</h3> 
<p>We think and act ahead to be the most effective and efficient.</p>

<h3> Improvement</h3> 
<p>We promote a learning environment by challenging new approaches to problem solving, exploring new methods and testing new ideas. Continuous improvement is evidenced in every facet of our business, from managing to controlling processes, from production to delivering. This powerful system encourages participation, maintains employee interest and improves overall morale. It sharpens the focus of the organization and contributes to improved quality, efficiency, and ultimately, a highly satisfied customer.</p>

<h3>Confidentiality</h3>

<p>We ensure your information of all type is handled in a confidential, secure and appropriate manner.</p>

<p><b>The key factors that have been bringing forth successes to our company include:</p></b>

<ul align="justify">
<li>Human Resource which comprises of highly experienced IT professionals who are dedicated to providing cost effective, high quality offshore outsourcing services.</li>
<li>Technical Skills in which re-using and continuously improving well-tested work is the key to cost effectiveness in software offshore outsourcing services in Prowesys. </li>
<li>Project Management: Transparent and professional project management environment has always been given the top priority by Prowesys to assure project missions are successfully accomplished. </li>
<li>Quality Management: Quality Management in Prowesys guarantees customers with effective and risks-free projects. </li>
<li>Infrastructure: Prowesys is ready to develop cross-platform applications in his modern, well-equipped and innovative infrastructure environment. </li>
<li>IP Protection: At Prowesys, information security management system makes sure that your information is distributed to authorized person only. </li>
</ul>

                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                        <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>About Company</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/about-us') }}">About Us</a></li>
									<li><a href="{{ url ('/industry') }}">Industry</a></li>
									<li><a href="{{ url ('/careers') }}">Careers</a></li>
									<li><a href="{{ url ('/contact-us') }}">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection
