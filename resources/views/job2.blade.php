@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/background/9.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Jobs</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
                    <h4><b>Systems Analysts</b></h4>
                   <h4> <a href="{{ url ('/jobs') }}"><u>Employment Opportunities</u></a></h4>
          
            </div>
        </div>
    </div>


            <section class="approach_area pad_btm">
                <div class="container">

                       
                        <div class="row">
                      
                             
                                        <table>
                                        <tbody>
                                        <tr>
                                        <th style="text-align: justify;">Posting Date:</th>
                                        <td style="padding-left: 3%; text-align: justify;">  28/04/2021</td>
                                        </tr>
                                        <tr>
                                        <th style="text-align: justify;">Position:</th>
                                        <td style="padding-left: 3%; text-align: justify;"> Systems Analysts </td>
                                        </tr>
                                        <tr>
                                        <th style="text-align: justify;">Employer:</th>
                                        <td style="padding-left: 3%; text-align: justify;">Prowesys Inc.</td>
                                        </tr>
                                        <tr>
                                        <th style="text-align: justify;">Job Location:</th>
                                        <td style="padding-left: 3%; text-align: justify;">Multiple locations</td>
                                        </tr>
                                        <tr>
                                        <th style="text-align: justify;">Min Education:</th>
                                        <td style="padding-left: 3%; text-align: justify;"> Bachelor Degree in Computer Science, Software Engineering, Information Technology/Systems or Equivalent.</td>
                                        </tr>
                                        <tr>
                                        <th style="text-align: justify;">Job Description:</th>
                                        <td style="padding-left: 3%; text-align: justify;"> Development and testing the Software Applications using Dataware housing reporting tools such as Cognos Report Studio, Query Studio, Analysis Studio, Cognos Work Space, Cognos Connection, Powerplay Transformer, Framework Manager, Active reports (Mobility), Cognos SDK, Oracle 10g, TOAD, PL/SQL Server, DB2, Crystal reports, JAVA Scripts, XML and Windows.</td>
                                        </tr>
                                        <tr>
                                        <th style="text-align: justify;">Apply by Mail:</th>
                                        <td style="padding-left: 3%; text-align: justify;">Send Resume to hr@prowesys.com or HR, 800 E Campbell Rd Ste 260, Richardson, TX 75081.</td>
                                        </tr>
                                        </tbody>
                                        </table>


                                        <hr>
                        </div>
                </div>
        </section>

    @endsection