@extends('layouts.app')

@section('content')

 <!--Page Title / Style Two-->
    <section class="page-title style-two" style="background-image:url(images/open.jpg)">
    	<div class="auto-container">
        	<h1 class="alternate">Technology</h1>
            <ul class="blog-info-post">
                <li>&nbsp;</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side / Blog Single-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                	<!--Blog Single-->
                	<div class="blog-single">
						<div class="inner-box">
                            <!--Title Box-->
                            <div class="title-box">
                            	<div class="title">Opensource</div>
                                <h2>We providing the highest quality of open source development services</h2>
                                
                            </div>
                            <!--Lower Box-->
                            <div class="lower-box">
                            	<p>PROWESYS as quality Offshore Development Center (ODC) offers a gamut of dedicated Open Source Software Development expertise namely PHP / MySQL, Ruby on Rails (RoR), AJAX, Joomla, WordPress and Drupal with quality programmers, developers and coders for clients spread across the globe.</p>
								
<p>We build and maintain a community of users and subscribers to create a successful open source development project. Our aim is to providing the highest quality of open source development services in every aspect of development. We put emphasis on not only delivering high quality open source solutions like PHP application development, Joomla development services, WordPress development, Drupal solutions but also establishing a long-term relationship with clients.</p>

<p>PROWESYS possesses the necessary expertise in developing offshore open source development solutions across a broad range of technologies. Our dedicated developers can integrate and customize various open source and Rich Internet Applications to develop quality web applications.</p>

<p>We offer our expertise in the below areas of open source development:</p>

<h3>Open Source Programming</h3>
o	PHP<br>
o	CakePHP<br>
o	MySQL<br>
o	MongoDB<br>
o	AJAX<br>
o	Ruby On Rails<br>
o	SOLR<br><br>
<h3>Open Source CMS</h3>
o	Joomla<br>
o	WordPress<br>
o	Drupal<br>
o	DotNetNuke<br><br>
<h3>Open Source E-Commerce Platforms</h3>
o	Magento<br>
o	OsCommerce<br>

<h3>Why PROWESYS for Open Source Development?</h3>

<p>We provide complete open source development solutions as per client requirements. You can also hire open source developers from PROWESYS at affordable price. In either case you are assured of the following benefits:</p>

o	Reduced Development cost and Development time by about 40%<br>
o	Innovative website development solutions<br>
o	Flexibility to meet certain needs<br>
o	Interactive development methodology to guarantee fully satisfactory result<br>
o	Improved portability<br>
o	Custom development and website integration, module features and different functionalities to improve the performance of your website and applications<br>
o	Understand the latest trends and technology<br>
o	Outstanding customer support<br>


                            </div>
                            
                          
                          
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar">
						
                      
                     
                        
                          <!-- Category List Widget -->
                        <div class="sidebar-widget-three category-list-widget">
                            <div class="sidebar-title-three"><h2>Technology</h2></div>
							<div class="widget-content">
                            	<ul>
                                	<li><a href="{{ url ('/technologies') }}">Overview</a></li>
									<li><a href="{{ url ('/oracle') }}">Oracle</a></li>
									<li><a href="{{ url ('/opensource') }}">Opensource</a></li>
									<li><a href="{{ url ('/sas') }}">SAS</a></li>
                                </ul>
                            </div>
                        </div>
                    
                       
                        
                        <!-- Testimonial Search -->
                        <div class="sidebar-widget-three testimonial-widget">
                        	<div class="widget-inner" style="background-image:url(images/resource/testimonial-3.jpg)">
                            	<div class="testimonial-widget-carousel owl-carousel owl-theme">
                                
                                	<!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We fulfill our commitments to our customers, our partners, shareholders, and each other. We take personal responsibility for our actions.</div>
                                            <div class="author">Commitment</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We value professionalism by all employees. We develop our staff to perform their work with expertise, dedication and care.</div>
                                            <div class="author">Professionalism</div>
                                        </div>
                                    </div>
                                    
                                    <!--Testimonial Block Widget-->
                                    <div class="testimonial-widget-block">
                                    	<div class="inner-box">
                                        	<div class="quote-icon">
                                            	<span class="icon flaticon-document"></span>
                                            </div>
                                            <div class="text">We think and act ahead to be the most effective and efficient.</div>
                                            <div class="author">Proactiveness</div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                      
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>
    <!--End Blog Small Section-->
      <section class="clients-section-two">
    	<div class="auto-container">
        	
		</div>
    </section>

@endsection