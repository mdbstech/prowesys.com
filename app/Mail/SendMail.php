<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\http\request;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(request $request)
    {
        return $this->view('mail',['txtName'=>$request->txtName,'txtPhone'=>$request->txtPhone,'txtEmail'=>$request->txtEmail,'txtSubject'=>$request->txtSubject,'txtComments'=>$request->txtComments,])->from($request->txtEmail)->to('jainsunaina.16@gmail.com');
    }
}

