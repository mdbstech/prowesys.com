<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SendMail;

class WebsiteController extends Controller
{
    public function home()
    {
    	return view ('home');
    }


    public function about()
    {
    	return view ('about');
    }

     public function contact()
    {
    	return view ('contact');
    }

    public function industry()
    {
    	return view ('industry');
    }

    public function careers()
    {
    	return view ('careers');
    }


//
    public function enterprise_mobility()
    {
        return view ('enterprise_mobility');
    }

    public function enterprise_app_management()
    {
        return view ('enterprise_app_management');
    }

    public function application_maintenace()
    {
        return view ('application_maintenace');
    }

    public function mechanical_services()
    {
        return view ('mechanical_services');
    }

    public function digital_marketing()
    {
        return view ('digital_marketing');
    }

    public function testing_services()
    {
        return view ('testing_services');
    }

    public function erp_practice()
    {
        return view ('erp_practice');
    }

    public function outsourcing()
    {
        return view ('outsourcing');
    }

    //

     public function technologies()
    {
        return view ('technologies');
    }


     public function oracle()
        {
            return view ('oracle');
        }


     public function opensource()
        {
            return view ('opensource');
        }


     public function sas()
        {
            return view ('sas');
        }
        
    //

         public function iot_development()
        {
            return view ('iot_development');
        }
         public function business_analytics()
        {
            return view ('business_analytics');
        }
         public function business_intelligence()
        {
            return view ('business_intelligence');
        }
         public function bigdata()
        {
            return view ('bigdata');
        }
         public function data_warehousing()
        {
            return view ('data_warehousing');
        }
         public function devops_aws()
        {
            return view ('devops_aws');
        }
     

        //

     public function send()
    {
     
      Mail::send(new SendMail());
      return back()->withSuccess('Your Message Sent Successfully..!! ');
      
    }


    
     public function jobs()
    {
        return view ('jobs');
    }

    public function job1()
    {
        return view ('job1');
    }

    public function job2()
    {
        return view ('job2');
    }

}
